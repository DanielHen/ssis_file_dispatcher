***Created by: Daniel Dabrowski dabrowski.daniel@interia.pl***

***Description:***
SSIS_Dispatcher has been created in order to demonstrate how to dispatches
files to different folders(based on file name). Feel free to use,modify this SSIS package.
It has been tested  on Microsoft SQL Serve 2016.


***How it works:***
SSIS pacgage  dispatches files to respective folders based on file name.

***Prerequisitions:*** 
Before initializing this package a proper folder scheme need to be created.
In order to do so please clone Python script(https://gitlab.com/DanielHen/flfo_create.git). Run Python script in order to obtaion a proper
file/folder scheme like below.


***INITIAL SCHEME***
<pre>
C:.
|
|
+---SSIS_Dispatcher
    |
    |
    +---FROM
    |   +---HOK20191201.txt
    |   +---HOK20191202.txt
    |   ....
    |   +---LARK20191201.txt
    |   +---LARK20191202.txt
    |    .... 
    |   +---MAT20191201.txt
    |   +---MAT20191202.txt
    |    .... 
    |   +---WKRM20191201.txt
    |   +---WKRM20191202.txt
    |    .... 
    |
    +---TO
    |   +---HOK
    |   +---LARK
    |   +---MAT
    |   +---WKRM
</pre>

***Explanation:***
In folder "FROM" there are all files. File name consist of report name like 'LARK' concatenate with
raport date e.g. LARK20200219.
Folder TO consistens of subfloders within. Folders name within are WKRM, LARK, MAT, HOK.
Package dispatches files based on report name to respective subfolders of TO folder.
The outcome folder/file  scheme will  look like this one below.

***RESULT SCHEME***

<pre>
C:.
|
|
+---SSIS_Dispatcher
    |
    |
    +---FROM
    |   +---HOK20191201.txt
    |   +---HOK20191202.txt
    |   ....
    |   +---LARK20191201.txt
    |   +---LARK20191202.txt
    |   .... 
    |   +---MAT20191201.txt
    |   +---MAT20191202.txt
    |    .... 
    |   +---WKRM20191201.txt
    |   +---WKRM20191202.txt
    |    .... 
    |
    +---TO
    |   +---HOK
    |   |    +---HOK20191201.txt
    |   |    +---HOK20191202.txt
    |   |    ....
    |   +---LARK
    |   |    +---LARK20191201.txt
    |   |    +---LARK20191202.txt 
    |   |    ....
    |   +---MAT
    |   |    +---MAT20191201.txt
    |   |    +---MAT20191202.txt
    |   |    .... 
    |   +---WKRM
    |   |   +---WKRM20191201.txt
    |   |   +---WKRM20191202.txt
    |   |   .... 
</pre>











